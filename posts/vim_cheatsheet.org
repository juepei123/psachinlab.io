#+title: VIM Cheatsheet
#+date: <2012-03-11>
#+keywords: vim cheatsheet commands
#+setupfile: ../org-templates/post.org

I moved to GNU Emacs but I maintain [[https://gitlab.com/psachin/latex/blob/master/vim_v2.pdf][VIM commands cheatsheet]]. I hope it is useful
to VIM users

#+ATTR_HTML: :width 100% :height
[[file:images/posts/vim_cheatsheet/vim_cheatsheet.png]]


#+INCLUDE: "../disquss.inc"
